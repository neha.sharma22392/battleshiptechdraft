from urllib2 import Request, urlopen, URLError
import urllib, urllib2, cookielib
import json
import httplib

board_id="test_board_1"
shot_column='A'
shot_row='1'

boardRequest = Request('https://student.people.co/api/challenge/battleship/74b467c6f087/boards')
singleBoardRequest=Request('https://student.people.co/api/challenge/battleship/74b467c6f087/boards/'+board_id)
singleShotUrl='https://student.people.co/api/challenge/battleship/74b467c6f087/boards/'

column=['A','B','C','D','E','F','G','H','I','J']
row=['1','2','3','4','5','6','7','8','9','10',]

boardId=['live_board_1','live_board_2','live_board_3','live_board_4','live_board_5','test_board_1','test_board_2','test_board_3','test_board_4','test_board_5',]
try:
	#Get respnses
	response = urlopen(singleBoardRequest)
	decodedResponse = response.read().decode('utf-8')
	json_obj = json.loads(decodedResponse)

	for item in boardId:
		singleShotUrl+item+"/"

		#Post: shot
		for r in row:
			for c in column:
				req = urllib2.Request(singleShotUrl+c+r, "")
				rsp = urllib2.urlopen(req)
				decodedResponse = rsp.read().decode('utf-8')
				json_obj = json.loads(decodedResponse)
				
				if (json_obj['is_hit']==True):
					nextr=int(r)
					for nextR in row[nextr:]:
						req = urllib2.Request(singleShotUrl+c+nextR, "")
						rsp = urllib2.urlopen(req)
						decodedResponse = rsp.read().decode('utf-8')
						json_obj = json.loads(decodedResponse)

						if (json_obj['is_hit']!=True):
							break

					nextc=int(column.index(c))
					for nextC in column[nextc:]:
						req = urllib2.Request(singleShotUrl+nextC+r, "")
						rsp = urllib2.urlopen(req)
						decodedResponse = rsp.read().decode('utf-8')
						json_obj = json.loads(decodedResponse)

						if (json_obj['is_hit']!=True):
							break


				#print decodedResponse
	
except URLError, e:
    print 'Error: ', e
